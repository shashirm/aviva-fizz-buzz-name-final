﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FizzBuzz.Models
{
    public class FizzBuzzModel
    {
        [Required(ErrorMessage = "Filling number is required")]
        [Range(1, 1000, ErrorMessage = "Number should be between 1 to 1000")]      
        public int num { get; set; }

        public string[] result { get; set; }

    }

    public class FizzBuzzResult
    {      

        public string getFizzBuzz(int num)
        {
            String FizzWizz = "Fizz";
            String BuzzWuzz = "Buzz";
            String result = "";

            // Check if today is Wednesday
            DateTime today = DateTime.Today;
            if ((int)today.DayOfWeek == (int)DayOfWeek.Wednesday)
            {
                FizzWizz = "Wizz";
                BuzzWuzz = "Wuzz";
            }


            if (num % 3 == 0) result = FizzWizz;

            if (num % 5 == 0) result = result + BuzzWuzz;

            if (result.Length == 0) result = num.ToString();

            return result;
        }
    }
}