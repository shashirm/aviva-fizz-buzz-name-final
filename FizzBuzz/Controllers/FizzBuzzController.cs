﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FizzBuzz.Models;

namespace FizzBuzz.Controllers
{
    public class FizzBuzzController : Controller
    {
        //
        // GET: /FizzBuzz/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index([Bind(Include="num")] FizzBuzzModel objNum)
        {
            
            if (ModelState.IsValid)
            {
                int i;
                i = Convert.ToInt32(objNum.num);

                FizzBuzzResult objResult = new FizzBuzzResult();

                objNum.result = new string[i];

                for (int j = 0; j < i; j++)
                {
                    objNum.result[j] = objResult.getFizzBuzz(j + 1);
                }


                return View(objNum); 
            }

            return View();
        }

        public ActionResult nextpage(int id)
        {
            int totalReords = Convert.ToInt32(Request.QueryString["TotalRecod"]);
            return View();
        }

    }
}
